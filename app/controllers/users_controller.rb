class UsersController < ApplicationController
  skip_before_filter :require_login
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]
  before_action :correct_user,   only: [:edit, :update]     
  before_action :admin_user,     only: [:index, :destroy]                                              

  def index
    @users = User.paginate(page: params[:page])
  end

  def show
    @user = User.find(params[:id])
    if current_user.admin? && params[:user_group_id]
      @user_invites = @user.memberships.where(user_id: @user.id, user_group_id: params[:user_group_id]).first
    end
  end

  def new
    @user = User.new
    @token = params[:invite_token] #<-- pulls the value from the url query string
  end
  
  def create
  @newUser = User.new(user_params)
  @newUser.activated = true
  @token = params[:invite_token]
    if @token != nil && @newUser.save
      @invite_email_and_token = Invite.find_by_token(@token)
      #user_unregistered_email = @invite_email_and_token.email
      user_submitted_email = params[:user][:email]
      #check if user submitted to same mail which was sent an invitation
      if @invite_email_and_token.email == user_submitted_email
        org = @invite_email_and_token.user_group
        @newUser.user_groups.push(org) #add this user to the new user group as a member
        redirect_to login_path
        flash[:success] = "You have successfully created an a account"
      elsif @invite_email_and_token.email != user_submitted_email
        @newUser.delete
        redirect_to :back
        flash[:danger] = "Please register for same email you got message"
      end
    else # if user doesn't have any invitation
      @user = User.new(user_params)
      if @user.save
        @user.send_activation_email
        flash[:notice] = "Please check your email to activate your account."
        redirect_to root_url
      else
        render 'new'
      end
    end
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.update_attributes(user_params)
      flash[:success] = "Profile updated"
      redirect_to @user
    else
      render 'edit'
    end
  end

  def destroy
    User.find(params[:id]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end

  private

  def user_params
  	params.require(:user).permit(:name, :email, :password, :password_confirmation)
  end

  # Confirms a logged-in user.
  def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
    end
  end

  def correct_user
    @user = User.find(params[:id])
    redirect_to(root_url) unless current_user?(@user)
  end

  def admin_user
    redirect_to root_url unless current_user.admin?
  end
end
