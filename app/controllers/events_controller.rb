class EventsController < ApplicationController
  skip_before_filter :nil_global
  before_action :set_event, only: [:show, :edit, :update, :destroy]
  before_action :set_user_group, only: [:new, :edit]
  before_action :get_id
  before_action :if_not_part_of_group
  before_action :not_creator, only: [:edit, :update, :destroy]


  # GET /events
  # GET /events.json
  def index
    @events = Event.where(user_group_id: $user_group_id)
    @group_name = UserGroup.find_by_id($user_group_id)
  end

  # GET /events/1
  # GET /events/1.json
  def show
  end

  # GET /events/new
  def new
    @event = Event.new
  end

  # GET /events/1/edit
  def edit
  end

  # POST /events
  # POST /events.json
  def create
    @event = Event.new(event_params)

    respond_to do |format|
      if @event.save
        #format.html { redirect_to @event, notice: 'Event was successfully created.' }
        format.html { redirect_to user_group_event_path(id: @event.id), notice: 'Event was successfully created.' }
        format.json { render :show, status: :created, location: user_group_event(id: @event.id) }
      else
        format.html { render :new }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /events/1
  # PATCH/PUT /events/1.json
  def update
    respond_to do |format|
      if @event.update(event_params)
        format.html { redirect_to user_group_event_path(id: @event), notice: 'Event was successfully updated.' }
        format.json { render :show, status: :ok, location: @event }
      else
        format.html { render :edit }
        format.json { render json: @event.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /events/1
  # DELETE /events/1.json
  def destroy
    @event.destroy
    respond_to do |format|
      format.html { redirect_to user_group_events_url, notice: 'Event was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_event
      @event = Event.find(params[:id])
    end

    def set_user_group
      @user_group = UserGroup.find_by_id(params[:user_group_id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def event_params
      params.require(:event).permit(:title, :description, :start_time, :end_time, :user_group_id, :creator_id)
    end

    def get_id
      if !params[:user_group_id].nil?
        $user_group_id = params[:user_group_id]
      end
    end

    def if_not_part_of_group
      @user_membership = current_user.memberships.where(user_id: current_user.id, user_group_id: $user_group_id).first

      if !@user_membership && !current_user.admin?
        redirect_to user_group_path(id: $user_group_id)
        flash[:alert] = "You are not member of this group"
      end 
    end

    def not_creator
      if current_user.id != @event.creator_id && !current_user.admin?
        flash[:danger] = "You are not event creator"
        redirect_to :back
      end
    end
end
