class InvitesController < ApplicationController
  before_action :set_invite, only: [:show, :edit, :update, :destroy]

  # GET /invites
  # GET /invites.json
  def index
    @invites = Invite.all
  end

  # GET /invites/1
  # GET /invites/1.json
  def show
  end

  # GET /invites/new
  def new
    @invite = Invite.new
  end

  # GET /invites/1/edit
  def edit
  end

  # POST /invites
  # POST /invites.json
  def create
    @invite = Invite.new(invite_params)
    @user_group = UserGroup.find(@invite.user_group_id)

    user_invites = current_user.memberships.where(user_group_id: @invite.user_group_id).first
    user_invites.update_attribute(:sent_at, Time.now)
    user_invites.increment!(:sent_invites)
    remain_invites = user_invites.max_invites - user_invites.sent_invites
    
    if user_invites.sent_invites <= user_invites.max_invites
      if @invite.save
        #if the user already exists
        if @invite.recipient != nil 
          #send a notification email
          @user_name = User.find(@invite.recipient_id)
          flash[:notice] = "Invitation sent, you have #{remain_invites} invites left"
          InviteMailer.existing_user_invite(@invite, @user_name, @user_group).deliver_now
          redirect_to user_group_path(@invite.user_group_id)
        else 
          flash[:notice] = "Invitation sent, you have #{remain_invites} invites left"
          InviteMailer.new_user_invite(@invite, signup_url(:invite_token => @invite.token), @user_group).deliver_now  #send the invite data to our mailer to deliver the email
          redirect_to user_group_path(@invite.user_group_id)
        end
      else     
        flash[:danger] = "Please enter valid e-mail"
        redirect_to edit_user_group_path(@invite.user_group_id)
      end
    else
      flash[:danger] = "You can't send more invitation requests."
      redirect_to user_group_path(@invite.user_group_id)
    end

   # if @invite.save
   #    #Mailer.invitation(@invitation, signup_url(@invitation.token)).deliver_now
   #    Invitation.invites(@invite, signup_url(:invite_token => @invite.token)).deliver_now  #send the invite data to our mailer to deliver the email
   #    redirect_to root_url
   # else
   #    # oh no, creating an new invitation failed
   # end
  end

  # PATCH/PUT /invites/1
  # PATCH/PUT /invites/1.json
  def update
    respond_to do |format|
      if @invite.update(invite_params)
        format.html { redirect_to @invite, notice: 'Invite was successfully updated.' }
        format.json { render :show, status: :ok, location: @invite }
      else
        format.html { render :edit }
        format.json { render json: @invite.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /invites/1
  # DELETE /invites/1.json
  def destroy
    @invite.destroy
    respond_to do |format|
      format.html { redirect_to invites_url, notice: 'Invite was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_invite
      @invite = Invite.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def invite_params
      params.require(:invite).permit(:email, :invite_token, :user_group_id, :sender_id, :recipient_id)
    end
end
