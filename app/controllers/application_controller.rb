class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper

before_filter :require_login
before_filter :nil_global


private

  def require_login
    unless current_user
      redirect_to login_url
      flash[:notice] = "Please log in"
    end
  end

  def nil_global
    if params[:user_group_id].nil?
      $user_group_id = nil
    end
  end
end
