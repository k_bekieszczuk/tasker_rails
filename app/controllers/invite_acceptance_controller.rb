class InviteAcceptanceController < ApplicationController

  def edit
    invite = Invite.find_by(token: params[:id])
    #invite.update_attribute(:accepted, false)
    if invite && !invite.accepted? && (current_user.id == invite.recipient_id)
      invite.update_attribute(:accepted,    true)
      invite.recipient.user_groups.push(invite.user_group)
      u_g_name = UserGroup.find(invite.user_group_id)
      flash[:success] = "Hello, welcome in #{u_g_name.name}"
      redirect_to user_group_path(invite.user_group_id)
    elsif invite && invite.accepted && (current_user.id == invite.recipient_id)
      flash[:notice] = "You are already part of this group"
      redirect_to user_group_path(invite.user_group_id)
    else
      flash[:danger] = "Invalid activation link"
      redirect_to root_url
    end
  end

  # def destroy
  #   invite = Invite.find_by(token: params[:id])

  #   if invite && !invite.accepted? && (current_user.id == invite.recipient_id)
  #     invite.delete
  #     flash[:success] = "Invite deleted"
  #     redirect_to root_url
  #   end
  # end
end
