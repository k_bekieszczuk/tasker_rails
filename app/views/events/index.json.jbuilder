json.array!(@events) do |event|
  json.extract! event, :id, :title, :description, :user_group_id, :creator_id
  json.start event.start_time
  json.end event.end_time
  json.user_group_id event.user_group_id
  json.url user_group_event_url(event.user_group_id, event.id, format: :html)
end