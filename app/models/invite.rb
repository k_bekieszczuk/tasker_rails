class Invite < ActiveRecord::Base
  belongs_to :user_group
  belongs_to :sender, :class_name => 'User'
  belongs_to :recipient, :class_name => 'User'

  before_create :generate_token
  before_save 	:check_user_existence
  before_save   :email_downcase

	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i
	validates :email, presence: true, 
					  length: { maximum: 255 },
					  format: { with: VALID_EMAIL_REGEX }
	

  def accept
		update_attribute(:accepted, true)
	end

  private

  def generate_token
  	self.token = Digest::MD5.hexdigest([self.user_group_id, Time.now, rand].join)
	end

	def check_user_existence
	  recipient = User.find_by_email(email)
	  if recipient
	    self.recipient_id = recipient.id
	  end
	end

	def email_downcase
		self.email = email.downcase
	end
end
