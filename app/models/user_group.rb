class UserGroup < ActiveRecord::Base
	has_many :memberships
    has_many :users, through: :memberships
    has_many :invites#, :class_name => "Invite", :foreign_key => 'user_group_id'
    has_many :events, inverse_of: :user_group, :dependent => :delete_all#, :class_name => "Event", :foreign_key => 'user_group_id'

    validates :name, presence: true, length: { maximum: 60 }, uniqueness: { case_sensitive: false }
    validates :user_id, presence: true

    accepts_nested_attributes_for :events
end
