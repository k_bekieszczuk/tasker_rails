class Membership < ActiveRecord::Base
    belongs_to :user
    belongs_to :user_group

    MAX_INVITES = 5

    def increment!(attribute, by = 1)
      increment(attribute, by).update_attribute(attribute, self[attribute])
    end
end
