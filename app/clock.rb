require './config/boot'
require './config/environment'
require 'clockwork'

module Clockwork
 every(1.day, 'delete.invite', :at => '00:00') {
   @invites = Invite.where("created_at >= ?", 2.week.ago.utc)
   @invites.each do |invite|
    invite.destroy
   end
 }
end
