class InviteMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.invitation.invites.subject
  #

  def new_user_invite(invite, signup_url, user_group)
    @invite = invite
    @signup_url = signup_url
    @user_group = user_group

    mail to: invite.email, subject: "TaskerNet invite"
  end

  def existing_user_invite(invite, user, user_group)
    @invite = invite
    @user = user
    @user_group = user_group
    #@accept_url = accept_url

    mail to: invite.email, subject: "Group invite"
  end

  #def invitation(invitation, signup_url)
  #  @invitation = invitation
  #  @signup_url = signup_url#

#    mail to: invitation.recipient_email
 #   invitation.update_attribute(:sent_at, Time.now)
  #end
end
