class AddMaxInvitesToMembership < ActiveRecord::Migration
  def change
  	add_column :memberships, :max_invites, :integer, null: false, default: 10
  end
end
