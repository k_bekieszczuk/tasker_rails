class AddSentInvitationsAndSentAtToMembership < ActiveRecord::Migration
  def change
  	add_column :memberships, :sent_invites, :integer, null: false, default: 0
  	add_column :memberships, :sent_at, :datetime
  end
end
