require 'test_helper'

class InvitationTest < ActionMailer::TestCase
  test "invites" do
    mail = Invitation.invites
    assert_equal "Invites", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
